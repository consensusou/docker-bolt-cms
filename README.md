Debian based Bolt CMS image.

Uses official PHP 7 Apache image - https://hub.docker.com/r/_/php/.

Apache configured to work with /var/www/bolt.

Bolt CMS pre-configured with "root" type user: admin/admin

Container exposes ports 80 and 443.

Triggering the following docker-compose config should spin up fully functional Bolt CMS locally and .

```YAML

version: '3'

services:
   app:
     image: consensusou/docker-bolt-cms:latest
     ports:
       - "80:80"
     restart: "no"
     
```

Work in Progress on PROD like image
